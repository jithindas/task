import React from 'react';
import { StyleSheet, View, Button } from 'react-native';


export default class Home extends React.Component {

    static navigationOptions = {
        title: 'Front Page',
    };

    render() {

        const { navigate } = this.props.navigation;

        return (
            <View style={styles.container}>

                <Button
                    title="Go to Second screen"
                    onPress={() => navigate(
                        'Second', { name: 'Second Page' }
                    )}
                />

            </View>
        );

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});