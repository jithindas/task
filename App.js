
import React from 'react';
import { View, Text, Button } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


import Front from './src/Front';
import Second from './src/Second';


const Navigator = createStackNavigator({
    Front: { screen: Front },
    Second: { screen: Second },
});
const App = createAppContainer(Navigator);

export default App;